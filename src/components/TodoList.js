
import React from 'react'
import PropTypes from 'prop-types'
import Todo from './Todo'

const TodoList = ( {todos} ) => (
  <div>
    {todos.map(todo => (
      <Todo key={todo.id} {...todo} />
    ))}
  </div>
)

TodoList.propTypes = {
  todos: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      text: PropTypes.string.isRequired
    }).isRequired
  ).isRequired
}

export default TodoList
