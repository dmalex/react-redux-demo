
import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, combineReducers } from 'redux'
import todos from './reducers/todos'
import App from './App'

ReactDOM.render(
  <Provider store={createStore(combineReducers({ todos }))}>
    <App />
  </Provider>,
  document.getElementById('root')
)
