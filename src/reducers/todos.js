
const todos = (state = [], action) => {
  if (action.type === 'ADD_TODO') {
    return [
      ...state,
      {
        id: action.id,
        text: action.text
      }
    ]
  } else {
    return state
  }
}
  
export default todos
